﻿// Learn more about F# at http://fsharp.org

open System


[<EntryPoint>]
let main argv =
    printfn "----------* Advent of Code *----------"
    printfn "----------* Day 1 *----------"

    printfn "Day 1 Part 1 is %i" AocDay1.Pt1.result
    AocDay1.Pt2.processSet
    
    printfn "----------* Day 2 *----------"
    printfn "Day 2 Part 1 is %i" AocDay2.Do.checksum

    0 // return an integer exit code
